#................................................................................................#
# Auditing and custom db role module                                                             #
#                                                                                                #
# audit_and_custom_db_role_main.tf: contains providers, variables and outputs definitions        #
#                                                                                                #
# Copyright © 2020 - present. Codertester.  All Rights Reserved.                                 #
#                                                                                                #
#................................................................................................#


# define provider(s) credential variables
variable "mongodb_atlas_public_key" {}
variable "mongodb_atlas_private_key" {}


# configure provider(s)
provider "mongodbatlas" {
  public_key   = "${var.mongodb_atlas_public_key}"
  private_key  = "${var.mongodb_atlas_private_key}"
}


#define non-provider variable(s)
variable "ids_of_existing_projects_for_auditing" {
  description = "A list of existing project ids of string type"
  default =   ["existing-org-id"]
}

variable "audit_filter" {
  description = "Check for successful authentication attempts: a json key-value pair. Each value is of string value"
  # Indicates if the auditing system captures successful authentication attempts for audit filters using the "atype" : "authCheck" auditing event
  default = "{ 'atype': 'authenticate', 'param': {   'user': 'audit_user_name',   'db': 'database_name',   'mechanism': 'SCRAM-SHA-1' }}"
}

variable "audit_authorization_success" {
  description = "Is JSON-formatted audit filter used by the project?"
  default = false
}

variable "enabled" {
  description = "Indicates if or not the project associated with an existing project_id, has database auditing enabled."
  default = true
}

variable "customized_role_name" {
  description = "The name of the customized role being created."
  default = "Specified_role_name"
}

variable "action_update" {
  description = "A UIR, etc or (DML equivalent in SQL) action:  insert, update, remove/delete, etc. on database and collection"
  # see - https://docs.atlas.mongodb.com/reference/api/custom-role-actions/
  default = "UPDATE"
}

variable "action_remove" {
  description = "A UIR, etc or (DML equivalent in SQL) action:  insert, update, remove/delete, etc. on database and collection"
  # see - https://docs.atlas.mongodb.com/reference/api/custom-role-actions/
  default = "REMOVE"
}

variable "action_insert" {
  description = "A UIR, etc or (DML equivalent in SQL) action:  insert, update or remove/delete, etc. on database and collection"
  # see - https://docs.atlas.mongodb.com/reference/api/custom-role-actions/
  default = "INSERT"
}

variable "collection_name" {
  default = "collection_name"
}

variable "database_name" {
  default = "db_name"
}


# create auditing(s) and customized role(s) for database user(s)
resource "mongodbatlas_auditing" "mongodb_auditing" {
  count                       = "${length(var.ids_of_existing_projects_for_auditing)}"
  project_id                  = "${var.ids_of_existing_projects_for_auditing[count.index]}"
  audit_filter                = "${var.audit_filter}"
  audit_authorization_success = "${var.audit_authorization_success }"
  enabled                     = "${var.enabled }"
}

resource "mongodbatlas_custom_db_role" "mongodb_custom_db_role" {
  count                       = "${length(var.ids_of_existing_projects_for_auditing)}"
  project_id                  = "${var.ids_of_existing_projects_for_auditing[count.index]}"
  role_name                   = "${var.customized_role_name}"

  actions {
    action                    = "${var.action_update}"
    resources {
      collection_name         = "${var.collection_name}"
      database_name           = "${var.database_name}"
    }
  }
  
  actions {
    action                    = "${var.action_insert}"
    resources {
      collection_name         = "${var.collection_name}"
      database_name           = "${var.database_name}"
    }
  }
  
  actions {
    action                    = "${var.action_remove}"
    resources {
      collection_name         = "${var.collection_name}"
      database_name           = "${var.database_name}"
    }
  }
}


#define output(s)
output "mongodbatlas_auditings" {
  description = "A list of all created MongoDB-Atlas auditings"
  #the list contains key-value pairs map of each auditing variables
  value = "${mongodbatlas_auditing.mongodb_auditing}"
}

output "mongodbatlas_custom_db_roles" {
  description = "A list of all created MongoDB-Atlas project maintenance windows"
  #the list contains key-value pairs map of each custom_db_role variables
  value = "${mongodbatlas_custom_db_role.mongodb_custom_db_role}"
}
