## MONGODB MIGRATION
---

This repo contains source codes for the migration of **On-Prem** MongoDB and other **external** MongoDB sources to cloud-based MongoDB Atlas.

---

## Terraform Template (TF)
1. Terraform template for the deployment of projects and project maintenance windows.
2. Terraform template for the deployment of clusters.
3. Terraform template for the deployment of alert configurations.
4. Terraform template for the deployment of auditing and custom db roles.
5. Terraform template for the deployment of private interface links.


## JavaScript Source Codes
1. Migration script for migrating MongoDB via mongodump and mongorestore.
2. Validation script for validating the source and target databases.
3. Parquet script for writing MongoDB's query output (JSON format) into PARQUET file format.

---
