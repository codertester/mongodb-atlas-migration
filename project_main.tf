#................................................................................................#
# Project and project maintenance window module                                                  #
#                                                                                                #
# project_main.tf: contains providers, variables and outputs definitions                         #
#                                                                                                #
# Copyright © 2020 - present. Codertester.  All Rights Reserved.                                 #
#                                                                                                #
#................................................................................................#


# define provider(s) credential variables
variable "mongodb_atlas_public_key" {}
variable "mongodb_atlas_private_key" {}


# configure provider(s)
provider "mongodbatlas" {
  public_key   = "${var.mongodb_atlas_public_key}"
  private_key  = "${var.mongodb_atlas_private_key}"
}


# define non-provider variable(s)
variable "org_id" {
  default = "existing-org-id"
}

variable "names_of_new_projects_to_be_created" {
  description = "A list of new projects names to be created of string type"
  default = ["project-1"]
}

variable "existing_users_team_id" {
  description = "ID of existing users team, on Mongo Atlas Dashboard, to be assigned to the newly created projects"
  default = "existing_team-id"
}

variable "role_names" {
  description = "A list of role(s) of string type"
  # can be one (below) or more than one role(s), e.g. ["GROUP_READ_ONLY", GROUP_DATA_ACCESS_READ_ONLY, "GROUP_DATA_ACCESS_READ_WRITE", etc]
  default = ["GROUP_READ_ONLY"]
}

variable "ids_of_existing_projects_for_maintenance" {
  description = "A list of existing project ids of string type"
  default =   ["existing-org-id"]
}

variable "day_of_week" {
  #day of the week to start maintenance window: S=1, M=2, T=3, W=4, T=5, F=6, S=7.
  default = 7 # every saturday
}

variable "hour_of_day" {
  #hour of the day to start maintenance window: 24-hour clock (UTC/GMT time zone), midnight=0, noon=12
  default = 1  # at 1:00 AM
}


# create project(s) and team(s)
# note: team(s) is/are optional, set create_team variable to true if desired
resource "mongodbatlas_project" "mongodb_project" {
  org_id        = "${var.org_id}"
  count         = "${length(var.names_of_new_projects_to_be_created)}"
  name          = "${var.names_of_new_projects_to_be_created[count.index]}"
  
  teams {
    team_id     = "${var.existing_users_team_id}"
    role_names  = "${var.role_names}"
  }
}

# create project maintenance window (s)
resource "mongodbatlas_maintenance_window" "mongodb_project_maintenance_window" {
  count         = "${length(var.ids_of_existing_projects_for_maintenance)}"
  project_id    = "${var.ids_of_existing_projects_for_maintenance[count.index]}"
  day_of_week   = "${var.day_of_week}"
  hour_of_day   = "${var.hour_of_day}"
}
  
#define output(s)
output "mongodbatlas_projects" {
  description = "A list of all created MongoDB-Atlas projects"
  #the list contains key-value pairs map of each projects variables
  value = "${mongodbatlas_project.mongodb_project}"
}

output "mongodbatlas_project_maintenance_windows" {
  description = "A list of all created MongoDB-Atlas project maintenance windows"
  #the list contains key-value pairs map of each project maintenance window variables
  value = "${mongodbatlas_maintenance_window.mongodb_project_maintenance_window}"
}