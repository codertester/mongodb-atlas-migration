/*
* ************************************************************************************************
*                                                                                                *
*  Copyright © 2020 - present. Codertester.  All Rights Reserved.                                *
*                                                                                                *
*                                                                                                *
*  This module (MongoDBParquet.js) implements MongoDBPParquet() class. The class is :            *                                                                                              *
*                                                                                                *
*   1)  Used for writing parquet file format (with json format as input).                        *
*       Input JSON could be output from:                                                         *
*       (a) MongoDB query                                                                        *
*.      (b) Any JSON object or                                                                   *
*       (c) Export of MongoDB collection (i.e. Table in RDBMS)                                   *
*                                                                                                *
*   2)  Used for reading parquet file format from SSD, HDD or object storage.                    *
*                                                                                                *                                                                                           *
*   3)  Implemented via Node.js and parquetjs module (https://www.npmjs.com/package/parquetjs).  *
*                                                                                                *
+ ************************************************************************************************
*/


class MongoDBParquet
{
  
    static customizedLongDateFormat()
    {
        return { weekday: "long", year: "numeric", month: "long", day: "numeric", hour: "2-digit", minute: "2-digit", second: "2-digit"};
    }
    
    static createJDataSetFromJsonObject(customized=false)
    {
        const parquet = require('parquetjs');
        
        // 1. declare a schema for the 'gas_field' table, with default encoding: see list of supported types
        const schema = new parquet.ParquetSchema(
        {
            gasFieldName: { type: 'UTF8'},
            ogipInTcf: { type: 'INT64' },
            priceInUSDPerMMscf: { type: 'DOUBLE'},
            discoveryDateISO: { type: 'TIMESTAMP_MILLIS'},
            discoveryDateLOCAL: { type: 'UTF8'},
            pipelineAccess: { type: 'BOOLEAN'},
            inProduction: { type: 'BOOLEAN'}
        });
          
        // 2. define json data
        let jsonData = [];
        let dateNowOneISO = new Date();
        let dateNowTwoISO = new Date();
        let dateNowOneLOCAL = undefined;
        let dateNowTwoLOCAL = undefined;
        
        if((customized === false) || (customized === undefined) || (customized === null))
        {
          dateNowOneLOCAL = (dateNowOneISO).toString();
          dateNowTwoLOCAL = (dateNowTwoISO).toString();
        }
        else if(customized === true)
        {
          dateNowOneLOCAL = (dateNowOneISO).toLocaleTimeString("en-us", MongoDBParquet.customizedLongDateFormat());
          dateNowTwoLOCAL = (dateNowTwoISO).toLocaleTimeString("en-us", MongoDBParquet.customizedLongDateFormat());
        }

        const inputJSONOne = {gasFieldName: 'ogbotobo', ogipInTcf: 4, priceInUSDPerMMscf: 2.5,
                              discoveryDateISO: dateNowOneISO, discoveryDateLOCAL: dateNowOneLOCAL,
                              pipelineAccess: true, inProduction: true};
        const inputJSONTwo = {gasFieldName: 'owopele', ogipInTcf: 2, priceInUSDPerMMscf: 2.5,
                              discoveryDateISO: dateNowTwoISO, discoveryDateLOCAL: dateNowTwoLOCAL,
                              pipelineAccess: true, inProduction: false};
                                
        jsonData.push(inputJSONOne);
        jsonData.push(inputJSONTwo);
        
        return {"inputSchema" : schema, "inputJson" : jsonData};

          /*
            List of Supported Types & Encodings (As at July 5th 2020)
            Reference: https://www.npmjs.com/package/parquetj
    
            Logical Type	    Primitive     Type	Encodings
            ===============================================
            UTF8	            BYTE_ARRAY	  PLAIN
            JSON	            BYTE_ARRAY	  PLAIN
            BSON	            BYTE_ARRAY	  PLAIN
            BYTE_ARRAY	      BYTE_ARRAY	  PLAIN
            TIME_MILLIS	      INT32	        PLAIN, RLE
            TIME_MICROS	      INT64	        PLAIN, RLE
            TIMESTAMP_MILLIS	INT64	        PLAIN, RLE
            TIMESTAMP_MICROS	INT64	        PLAIN, RLE
            BOOLEAN	          BOOLEAN	      PLAIN, RLE
            FLOAT	            FLOAT	        PLAIN
            DOUBLE	          DOUBLE	      PLAIN
            INT32	            INT32	        PLAIN, RLE
            INT64	            INT64	        PLAIN, RLE
            INT96	            INT96	        PLAIN
            INT_8	            INT32	        PLAIN, RLE
            INT_16	          INT32	        PLAIN, RLE
            INT_32	          INT32	        PLAIN, RLE
            INT_64	          INT64	        PLAIN, RLE
            UINT_8	          INT32	        PLAIN, RLE
            UINT_16	          INT32	        PLAIN, RLE
            UINT_32	          INT32	        PLAIN, RLE
            UINT_64	          INT64	        PLAIN, RLE
          */
    }
    
    static createJDataSetFromMongoQuery()
    {
      //implement this and return its value to replace lines 126, 127 and 128 :-> if this is the source of JSON input
    }
    
    static createJDataSetFromMongoCollectionExport()
    {
       //implement this and return its value to replace lines 126, 127 and 128 :-> if this is the source of JSON input
    }
        
    async writeJsonToParquetFile(inputSchema=undefined, inputJson=undefined, filename=undefined)
    {
        const parquet = require('parquetjs');
        
        // define input arguments with test datasets, if undefined,
        if((inputSchema === undefined) &&  (inputJson === undefined) && (filename === undefined))
        {
          const customized = false;
          const cdst  = MongoDBParquet.createJDataSetFromJsonObject(customized);
          inputSchema = cdst.inputSchema;
          inputJson = cdst.inputJson;
          filename = 'gas_field.parquet'; //create file in current working directory (CWD)
        }
        
        // create new ParquetWriter that writes to 'gas_field.parquet'
        let writer = await parquet.ParquetWriter.openFile(inputSchema, filename);
        
        //buffer size: (1) controls max. no of rows buffered in memory & (2) no. of rows co-located on disk; default value is: 4096
        writer.setRowGroupSize(8192);
        
        // append rows to the file
        await writer.appendRow(inputJson[0]);
        await writer.appendRow(inputJson[1]);
      
        //close() to avoid leaking file descriptors
        await writer.close();
        
        //confirm file creation
        console.log("------------------------------------------------");
        console.log("File (", filename, ") successfully created.");
        console.log("------------------------------------------------");
        console.log("");
    }
    
    async readWholeParquetFile(file=undefined)
    {
        const parquet = require('parquetjs');
        
        // create new ParquetReader that reads from file
        let reader = await parquet.ParquetReader.openFile(file);
         
        // create a new cursor that loads all records
        let cursor = reader.getCursor();
        
        // read all records from the file and print them
        let record = null;
        while (record = await cursor.next())
        {
          console.log(record);
        }

        //close() to avoid leaking file descriptors
        await reader.close();
        
        //confirm file is read
        console.log("--------------------------------------------------");
        console.log("Full file (", file, ") successfully read.");
        console.log("--------------------------------------------------");
        console.log("");
    }
    
    async readSomeColumnFromParquetFile(file=undefined)
    {
        const parquet = require('parquetjs');
        
        // create new ParquetReader that reads from filePath
        let reader = await parquet.ParquetReader.openFile(file);
         
        // create a new cursor that loads just 'gasFieldName' and 'priceInUSDPerMMscf' columns
        let cursorSomeColumns = reader.getCursor(['gasFieldName', 'priceInUSDPerMMscf']);
        
        let someRecord = null;
        while (someRecord = await cursorSomeColumns.next()) {
          console.log(someRecord);
        }

        //close() to avoid leaking file descriptors
        await reader.close();
        
        //confirm file is partially read
        console.log("-----------------------------------------------------");
        console.log("Part of file (", file, ") successfully read.");
        console.log("-----------------------------------------------------");
        console.log("");
    }
}


// invoke test as IIFE, if desired
const test = false;

if(test === true)
{
  (function testMongoDBParquet()
  {
    const mdbpqt = new MongoDBParquet();
    let createFile = mdbpqt.writeJsonToParquetFile(inputSchema=undefined, inputJson=undefined, filename=undefined);
    
    createFile.then(function()
    {
      mdbpqt.readWholeParquetFile(file='gas_field.parquet');
    }).then(function()
    {
      mdbpqt.readSomeColumnFromParquetFile(file='gas_field.parquet');
    }).catch(function(error)
    {
      console.log(error);
    });
  })();
}


// export module so that it can be used by other module or application
module.exports = {MongoDBParquet};