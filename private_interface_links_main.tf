#................................................................................................#
# Private interface links module                                                                 #
#                                                                                                #
# private_interface_links_main.tf: contains providers, variables and outputs definitions         #
#                                                                                                #
# Copyright © 2020 - present. Codertester.  All Rights Reserved.                                 #
#                                                                                                #
#................................................................................................#


# define provider(s) credential variables
variable "mongodb_atlas_public_key" {}
variable "mongodb_atlas_private_key" {}
variable "aws_access_ke" {}
variable "aws_secret_key" {}
variable "aws_region" {}

# configure provider(s)
provider "mongodbatlas" {
  public_key   = "${var.mongodb_atlas_public_key}"
  private_key  = "${var.mongodb_atlas_private_key}"
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}


# define non-provider variable(s)
variable "ids_of_existing_projects_for_auditing" {
  description = "A list of existing project ids of string type"
  default =   ["existing-org-id"]
}

variable "provider_name" {
 default = "AWS"
}

variable "region" {
 default = "region-value"
}

variable "vpc_id" {
 default = "vpc-value" 
}

variable "vpc_endpoint_type" {
  default = "Interface"
}

variable "subnet_ids" {
 description = "A list of subnet ids of string type"
 default = ["subnet-value"]  
}

variable "security_group_ids" {
 description = "A list of subnet group ids of string type"
 default = ["sg-value"] 
}


# mongodb atlas private endpoint(s)
resource "mongodbatlas_private_endpoint" "mongodb_pept" {
  count           = "${length(var.project_ids)}"
  project_id      = "${var.project_ids[count.index]}"
  provider_name   = "${var.provider_name}"
  region          = "${var.region}"
}

# aws vpc endpoint(s)
resource "aws_vpc_endpoint" "aws_vpc_ept" {
  count              = "${length(var.project_ids)}"
  vpc_id             = "${var.vpc_id}"
  service_name       = "${mongodbatlas_private_endpoint.mongodb_pept[count.index].endpoint_service_name}"
  vpc_endpoint_type  = "${var.vpc_endpoint_type}"
  subnet_ids         = "${var.subnet_ids}"
  security_group_ids = "${var.security_group_ids}"
}

# mongodb atlas private endpoint interface link(s)
resource "mongodbatlas_private_endpoint_interface_link" "mongodb_pept_link" {
  count                 = "${length(var.project_ids)}"
  project_id            = "${var.project_ids[count.index]}"
  private_link_id       = "${mongodbatlas_private_endpoint.mongodb_pept[count.index].private_link_id}"
  interface_endpoint_id = "${aws_vpc_endpoint.aws_vpc_ept[count.index].id}"
}


# define output(s)
output "mongodbatlas_private_endpoints" {
  description = "A list of all created MongoDB-Atlas private endpoints"
  # the list contains key-value pairs map of each endpoint variables
  value = "${mongodbatlas_private_endpoint.mongodb_pept}"
}

output "mongodbatlas_private_endpoint_interface_links" {
  description = "A list of all created MongoDB-Atlas private endpoints interface links"
  # the list contains key-value pairs map of each interface link variables
  value = "${mongodbatlas_private_endpoint_interface_link.mongodb_pept_link}"
}