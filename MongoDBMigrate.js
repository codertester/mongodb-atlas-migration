/*
 *****************************************************************************************************
 *                                                                                                   *
 *  Migration of MongoDB Database.                                                                   *
 *                                                                                                   *
 *  Copyright © 2020 - present. Codertester.  All Rights Reserved.                                   *
 *                                                                                                   *
 *                                                                                                   *
 *  This module (MongoDBMigrate.js) implements MongoDBMigrate() class. The class is :                *                                                                                              *
 *                                                                                                   *
 *   1)  Used for migrating a MongoDB database from a source to a target MongoDB Server.             *
 *                                                                                                   *
 *   2)  Based on the MongoDB "mongodump" and "mongorestore" commands                                *
 *                                                                                                   *                                                                                           *
 *   3)  Implemented via Node.js & MongoDB Database Tools (https://docs.mongodb.com/database-tools/) *                                         *
 *                                                                                                   *
 *****************************************************************************************************
*/


class MongoDBMigrate
{
    constructor()
    {
      return null;
    }
    
    connectAndMigrate(connectingCredentials=null, dataPath=null, migrationOption=null, mongoDBAtlas=false, collection=null)
    {
        //declare and define common variables
        const assert  = require('assert');
        const childProcess = require('child_process');
        const optionExec = {timeout: 7200000}; //2 hour time-out
        const cds = connectingCredentials;
        let dumpCommand = null;
        let restoreCommand = null;

        if(migrationOption === "dump")
        {
            if(mongoDBAtlas === true)
            {
              if(cds.dbSsl === false)
              {
                 dumpCommand = "sudo mongodump --uri=mongodb+srv://" + cds.dbUserName + ":" + cds.dbUserPassword +
                               "@" + cds.dbDomainURL + "/" + cds.dbName  + " --out /" + dataPath;
              }
              else if(cds.sslOptions === true)
              {
                 return console.log("Use non-ssl option to dump data from MongoDB Atlas")
              }
            }
            else
            {
              if(cds.dbSsl === false)
              {
                dumpCommand = "sudo mongodump --port " + cds.dbPort + " -u " +  cds.dbUserName + " -p " + cds.dbUserPassword +
                               " --authenticationDatabase " + cds.dbName + " --host " + cds.dbDomainURL +  " --out /" + dataPath;
              }
              else if(cds.sslOptions === true)
              {
                dumpCommand = "sudo mongodump --port " + cds.dbPort + " -u " +  cds.dbUserName + " -p " + cds.dbUserPassword +
                              " --authenticationDatabase " + cds.dbName + " --ssl --sslPEMKeyFile " + cds.sslOptions.key +
                              " --sslCAFile " + cds.sslOptions.key  + " --host " + cds.dbDomainURL +  " --out /" + dataPath;
              }
            }

            childProcess.execSync(dumpCommand, optionExec);
            console.log("MongoDB dump is now completed!");
        }
        else if(migrationOption === "restore")
        {
            if(mongoDBAtlas === true)
            {
              if(cds.dbSsl === false)
              {
                 restoreCommand = "sudo mongorestore --uri=mongodb+srv://" + cds.dbUserName + ":" + cds.dbUserPassword + "@" +
                                   cds.dbDomainURL + "/" + cds.dbName  + " /" + dataPath + cds.dbName + "/" + collection + ".bson";
              }
              if(cds.dbSsl === true)
              {
                 return console.log("Use non-ssl option to restore data to MongoDB Atlas")
              }
            }
            else
            {
              if(cds.dbSsl === false)
              {
                restoreCommand = "sudo mongorestore --port " + cds.dbPort + " -u " +  cds.dbUserName + " -p " + cds.dbUserPassword +
                                 " --authenticationDatabase " + cds.dbName  + " --host " + cds.dbDomainURL + " /" + dataPath +
                                 cds.dbName + "/" + collection + ".bson";
              }
              if(cds.dbSsl === true)
              {
                restoreCommand = "sudo mongorestore --port " + cds.dbPort + " -u " +  cds.dbUserName + " -p " + cds.dbUserPassword +
                                 " --authenticationDatabase " + cds.dbName  + " --ssl --sslPEMKeyFile " + cds.sslOptions.key +
                                 " --sslCAFile " + cds.sslOptions.key  + " --host " + cds.dbDomainURL + " /" + dataPath +
                                 cds.dbName + "/" + collection + ".bson";
              }
            }
            
            childProcess.execSync(restoreCommand, optionExec);
            console.log("MongoDB restore is now completed!");
        }
        else
        {
          console.log("No MongoDB migration option is selected: 'dump' or 'restore' must be selected!");
          return;
        }
    }
    
    testMongoDBMigrate()
    {
        // connecting credentials
        // note: for "restore", "dbName" must the database name (i.e. same as folder name), to be restored, from the "dataPath/" below (With Forward Slash: /)
        // note: for "dump", "dbName" is the database name, to be dumped, into the "dataPath" below (No Forward Slash: /)
        let dbPort         = "27017";
        let dbName         = "dbName";
        let dbUserName     = "dbUserName";
        let dbUserPassword = "dbUserPassword";
        let dbDomainURL    =  "dbDomainURL";
        let dbSsl          = false;
        let sslOptions     = null;
        if(dbSsl === true)
        {
          sslOptions = {"ca": "/pathToCAFile/caFile.pem" , "key": "/pathToKEYFile/mongodb.pem", "cert": "/pathToCERTFile/certFile.pem"};
        }
        
        let connectingCredentials = {"dbPort" : dbPort, "dbName" : dbName, "dbUserName" : dbUserName,
                                    "dbUserPassword" : dbUserPassword, "dbDomainURL" : dbDomainURL,
                                    "dbSsl" : dbSsl, "sslOptions" : sslOptions};
     
        //migrationOption, dataPath, mongoDBAtlas confirmation and collection specification
        let migrationOption = "dump"; // or "restore";
        let dataPath  = null;
        let mongoDBAtlas = false; // confirm if connecting to database is mongoDBAtlas DBaaS
        let collection = null;
        
        if(migrationOption === "dump")
        {
          dataPath  = "path_to_dump_or_restore/dump_folder";
        }
        else if(migrationOption === "restore")
        {
          dataPath  =  "path_to_dump_or_restore/dump_folder/";
          
          if(mongoDBAtlas === true)
          {
            // specify a single colllection at a time, in the dataPath
            collection = "collectionName"
          }
          else if(mongoDBAtlas === false)
          {
            // specify all available colllections, in the dataPath
            collection = "*";
          }
        }
        
        //finally pass in credentials, data path, migration option, mongoDBAtlas confirmation and collection
        let mdbv = new MongoDBMigrate();
        mdbv.connectAndMigrate(connectingCredentials=connectingCredentials, dataPath=dataPath,
                               migrationOption=migrationOption, mongoDBAtlas=mongoDBAtlas,
                               collection=collection)
    }
}


// invoke test as IIFE, if desired
const test = false;

if(test === true)
{
  (function testMongoDBParquet()
  {
    let migrate = new MongoDBMigrate().testMongoDBMigrate();
  })();
}

module.exports = {MongoDBMigrate};