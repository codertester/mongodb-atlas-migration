#................................................................................................#
# Alert configuration module                                                                     #
#                                                                                                #
# alert_config_main.tf: contains providers, variables and outputs definitions                    #
#                                                                                                #
# Copyright © 2020 - present. Codertester.  All Rights Reserved.                                 #
#                                                                                                #
#................................................................................................#

# define provider(s) credential variables
variable "mongodb_atlas_public_key" {}
variable "mongodb_atlas_private_key" {}


# configure provider(s)
provider "mongodbatlas" {
  public_key   = "${var.mongodb_atlas_public_key}"
  private_key  = "${var.mongodb_atlas_private_key}"
}


#define non-provider variable(s)
# 1. general variables
variable "project_id" {
  description = "Existing project id, where the alert configuration will be created."
  default = "project-1"
}

variable "event_type" {
  description = "Type of event to be triggered an alert."
  default = "OUTSIDE_METRIC_THRESHOLD"
}

variable "enabled" {
  description = "Indicates if the alert configuration is enabled or not."
  default = true
}


# 2. notification variables
variable "type_name" {
  description = "Type of alert notification."
  # "GROUP" value below indicates project-level alert
  # other alternative values that can be selected, which are relevant to current module include: "ORG" and  "TEAM"
  default = "GROUP" # i.e. project-level notification
}

variable "interval_min" {
  description = "Number of minutes between unacknowledged alerts that are not resolved. Minimum value is 5."
  default = 5  # should be greated greater or equal to 5 minutes
}

variable "delay_min" {
  description = "Number of minutes to wait before sending alert notification, when an alert condition is detected."
  default = 0
}

variable "sms_enabled" {
  description = "Indicates if sms notification is enabled or not."
  # mobile_number variable must be specified for this variable (see next variable)
  default = true
}

variable "mobile_number" {
  description = "Mobile number for sms alert notifications."
  # required if sms notification is enabled (see above variable)
  default = "123-456-7890"
}

variable "email_enabled" {
  description = "Indicates if email notification is enabled or not."
  # email_address variable must be specified for this variable (see next variable)
  default = true
}

variable "email_address" {
  description = "Email address for alert notifications."
  # required if email notification is enabled (see above variable)
  # default = "info@domainname.com"
}


# 3. matching variables
# a. non-nested
variable "field_name" {
  description = "Name of the field in the target object to match."
  # other valid values include: REPLICA_SET_NAME, CLUSTER_NAME, HOSTNAME_AND_PORT, etc..
  default = "TYPE_NAME"
}

variable "matcher_operator" {
  description = "Operator to test the field’s value. If omitted, the configuration is disabled"
  # other valid values include: NOT_EQUALS, CONTAINS, NOT_CONTAINS, STARTS_WITH, ENDS_WITH & REGEX
  default = "EQUALS"
}

variable "value" {
  description = "Value to test with the specified operator. If omitted, the configuration is disabled.:"
  default = "PRIMARY"
}


# b. nested
variable "metric_name" {
  description = "Name of the metric to check."
  default = "ASSERT_REGULAR"
}

variable "metric_threshold_operator" {
  description = "Operator for checking metric value against the threshold value."
  # other valid value include: GREATER_THAN
  default = "LESS_THAN"
}

variable "threshold" {
  description = "Threshold value for alert trigger."
  default = 99
}

variable "units" {
  description = "The units for the threshold value. It depends on the type of metric."
   # other valid values, based on size, include: BITS, BYTES, KILOBITS, KILOBYTES, MEGABITS, MEGABYTES, GIGABITS, GIGABYTES, TERABYTES & PETABYTES
   # other valid values, based on time, include: MILLISECONDS, SECONDS, MINUTES, HOURS & DAYS
  default = "RAW"
}

variable "mode" {
  description = "The mode of computation for the metric. This must be set to AVERAGE"
  # MongodDB Atlas computes the metric value as an average.
  default = "AVERAGE"
}


# create alert configuration(s)
resource "mongodbatlas_alert_configuration" "mongodb_alert_configuration" {
  project_id      = "${var.project_id}"
  event_type      = "${var.event_type}"
  enabled         = "${var.enabled}"

  notification {
    type_name     = "${var.type_name}"
    interval_min  = "${var.interval_min}"
    delay_min     = "${var.delay_min}"
    sms_enabled   = "${var.sms_enabled}"
    mobile_number = "${var.mobile_number}"
    email_enabled = "${var.email_enabled}"
    email_address = "${var.email_address}"
  }

  # rules for matching an object against the alert configuration
  matcher {
    field_name    = "${var.field_name}"
    operator      = "${var.matcher_operator}"
    value         = "${var.value}"
  }

  # threshold alert trigger: required if event_type_name is "OUTSIDE_METRIC_THRESHOLD"
  metric_threshold = {
    metric_name   = "${var.metric_name}"
    operator      = "${var.metric_threshold_operator}"
    threshold     = "${var.threshold}"
    units         = "${var.units}"
    mode          = "${var.mode}"
  }
}
  

#define output(s)
output "mongodb_alert_configuration_outputs" {
  description = "A list containing (1) event-type and (2) a map of notification properties for the created alert configuration."
  value = ["${mongodbatlas_alert_configuration.mongodb_alert_configuration.event_type}",
           "${mongodbatlas_alert_configuration.mongodb_alert_configuration.notification}",
          ]
}