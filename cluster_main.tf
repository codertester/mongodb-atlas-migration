#................................................................................................#
# Cluster module                                                                                 #
#                                                                                                #
# cluster_main.tf: contains providers, variables and outputs definitions                         #
#                                                                                                #
# Copyright © 2020 - present. Codertester.  All Rights Reserved.                                 #
#                                                                                                #
#................................................................................................#

# define provider(s) credential variables
variable "mongodb_atlas_public_key" {}
variable "mongodb_atlas_private_key" {}


# configure provider(s)
provider "mongodbatlas" {
  public_key   = "${var.mongodb_atlas_public_key}"
  private_key  = "${var.mongodb_atlas_private_key}"
}


#define non-provider variable(s)
variable "project_ids" {
  description = "A list of existing project ids of string type"
  default =   ["existing-org-id"]
}

variable "cluster_names" {
 description = "A list of cluster names of string type"
 # the length of cluster_names must be equal to the length of project_ids
 default = ["cluster-0"]
}

variable "num_shards" {
 default = 1
}

variable "replication_factor" {
 default = 3
}

variable "provider_backup_enabled" {
 default = true
}

variable "auto_scaling_disk_gb_enabled" {
 default = true
}

variable "disk_size_gb" {
 # this is the minimum size cluster that can be created via Terraform module
 # this cluster option has the following specs : 10GB storage, 2GB RAM, 2vCPUs and 100 IOPS
 default = 10
}

variable "mongo_db_major_version" {
 default = "4.2"
}

variable "provider_name" {
 default = "AWS"
}

variable "provider_disk_iops" {
 default = 100
}

variable "provider_volume_type" {
 default = "STANDARD"
}

variable "provider_encrypt_ebs_volume" {
 default = true
}

variable "provider_instance_size_name" {
 default = "M10"
}

variable "provider_region_name" {
 default = "US_EAST_1"
}


# create single region cluster(s)
resource "mongodbatlas_cluster" "mongodb_cluster" {
  #define cluster(s)
  count                         = "${length(var.project_ids)}"
  project_id                    = "${var.project_ids[count.index]}"
  name                          = "${var.cluster_names[count.index]}"
  num_shards                    = "${var.num_shards}"

  #cluster(s) sizing and version
  replication_factor            = "${var.replication_factor}"
  provider_backup_enabled       = "${var.provider_backup_enabled}"
  auto_scaling_disk_gb_enabled  = "${var.auto_scaling_disk_gb_enabled}"
  disk_size_gb                  = "${var.disk_size_gb}"
  mongo_db_major_version        = "${var.mongo_db_major_version}"

  #provider settings
  provider_name                 = "${var.provider_name}"
  provider_disk_iops            = "${var.provider_disk_iops}"
  provider_volume_type          = "${var.provider_volume_type}"
  provider_encrypt_ebs_volume   = "${var.provider_encrypt_ebs_volume}"
  provider_instance_size_name   = "${var.provider_instance_size_name}"
  provider_region_name          = "${var.provider_region_name}"
}


#define output(s)
output "mongodbatlas_clusters" {
  description = "A list of all created MongoDB-Atlas clusters"
  #the list contains key-value pairs map of each cluster variables
  value = "${mongodbatlas_cluster.mongodb_cluster}"
}
