/*
 **************************************************************************************************
 *                                                                                                *
 *  Validation of MongoDB Migrated Data.                                                          *
 *                                                                                                *
 *  Copyright © 2020 - present. Codertester.  All Rights Reserved.                                *
 *                                                                                                *
 *                                                                                                *
 *  MongoDBValidateNode.js implements MongoDBValidate() class. This is used for:                  *
 *                                                                                                *
 *  Validating migrated MongoDB data source and its target with:                                  *
 *  1) MongoDB Native Driver and                                                                  *
 *  2) Node.js - A JavaScript runtime                                                             *
 *                                                                                                *
 *  Target: MongoDB Atlas Replicas Set                                                            *
 *                                                                                                *
 *  Source: On Prem, AWS-based, other cloud providers-based or any other external MongoDB Server. *
 *                                                                                                *
 *  References: 1) MongoDB Native Driver - https://mongodb.github.io/node-mongodb-native/         *
 *.             2) Node.js - https://nodejs.org/en/download/                                      *
 *                                                                                                *
 *  How to use this module:                                                                       *
 *  -----------------------                                                                       *
 *   (1) Ensure Node.js and Mongodb native driver are installed:                                  *
 *     (a) Download Node.js from: https://nodejs.org/en/download/  and install.                   *
 *     (b) Then install "MonogoDB Native Driver with this command:  npm install mongodb --save.   *
 *   (2) Fill in source & target database credentials under "testMongoDBValidate()" method below. *
 *   (3) Uncomment line 333 in this file - see below.                                             *
 *   (4) Then type in following command and Press ENTER:                                          *
 *       node /path_to_file/MongoDBValidateNode.js"                                               *
 *       where:                                                                                   *
 *        a. MongoDBValidateWithNode.js = name of this file/script.                               *
 *        b. path_to_file = the location of MongoDBValidateNode.js file.                          *
 *   (5) Then, check a file (MongoDBValidate.txt) in the current working directory (CWD) for      *
 *       the validation results. Anytime a run is made, results are appended to this file.        *
 *       The results should also be printed onto the console anytime a run is made.               *
 **************************************************************************************************
*/

class MongoDBValidate
{
    constructor()
    {
      return null;
    }
    
    static saveValidationResults(targetStats, sourceStats, confirm, resultsFileName)
    {
        let fs   = require('fs');
        let util = require('util');
        fs.writeFileSync(resultsFileName,  "\n" + "--------------------------------------------------------------------------", {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n" + "Start Time: " + new Date(), {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n" + "--------------------------------------------------------------------------", {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n" + "Target Validation Stats" + "\n", {flag: 'a'});
        fs.writeFileSync(resultsFileName, util.format(targetStats), {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n", {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n" + "Source Validation Stats" + "\n", {flag: 'a'});
        fs.writeFileSync(resultsFileName, util.format(sourceStats), {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n" + "--------------------------------------------------------------------------", {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n" + "Is validation okay?: " + confirm, {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n" + "--------------------------------------------------------------------------", {flag: 'a'});
        fs.writeFileSync(resultsFileName, "\n" + "\n", {flag: 'a'});
    }

    validate(targetStats, sourceStats)
    {
        //remove db.stats' variables not used in validation
        let usedStat = MongoDBValidate.prototype.removeUnusedStatistics(targetStats, sourceStats);
        let _targetStats = targetStats;
        let _sourceStats = sourceStats;
        
        //get db.stats' variables used (relevant) in validation
        let validationList = MongoDBValidate.prototype.getRelevantStatistics(targetStats, sourceStats);
        let targetValidationVariablesList = validationList.targetValidationVariablesList;
        let sourceValidationVariablesList = validationList.sourceValidationVariablesList;
      
        // validate migrated database using relevant db.stats' variables
        console.log();
        console.log("------------------------------------------");
        console.log("Starting Validation Engine.");
        let confirm = MongoDBValidate.prototype.compareStatistics(targetValidationVariablesList, sourceValidationVariablesList);
        
        //console.log input validation variables
        console.log("------------------------------------------");
        console.log("Target Validation Statistics: " );
        console.log(_targetStats);
        console.log("------------------------------------------");
        console.log("Source Validation Statistics: ");
        console.log(_sourceStats);
        console.log("------------------------------------------");
        
        //print validation results
        if(confirm  === true)
        {
          console.log("Is validation okay?");
          console.log("Yes, " + "'" + _sourceStats.db + "' database is validated");
        }
        else
        {
          console.log("Is validation okay?");
          console.log("No, " + "'" + _sourceStats.db + "' database is NOT validated");
        }
        
        console.log("------------------------------------------");
        console.log("End of Validation Engine.");
        console.log("------------------------------------------");

        //finally, save stats to file
        let resultsFileName = "MongoDBValidate.txt";
        MongoDBValidate.saveValidationResults(_targetStats, _sourceStats, confirm, resultsFileName);
    }
  
    compareStatistics(targetValidationVariablesList, sourceValidationVariablesList)
    {
        let count  = 0;
        let targetLength = targetValidationVariablesList.length;
        let sourceLength = sourceValidationVariablesList.length;
        let confirm = false;
        
        if(targetLength  === sourceLength)
        {
            for(let index = 0; index < targetLength; index++)
            {
                if(targetValidationVariablesList[index] !== sourceValidationVariablesList[index])
                {
                    count = count + 1;
                }
            }
                              
            if(count > 0)
            {
                confirm = false;
                return confirm;
            }
            
            confirm = true;
            return confirm;
        }
        
        return confirm;
    }
    
    mongoDBConnectionOptions(sslCertOptions, enableSSL)
    {
        if(enableSSL === true)
        {
            return {useNewUrlParser: true, useUnifiedTopology: true, readPreference: 'primaryPreferred',
                    maxStalenessSeconds: 90, poolSize: 200, ssl: true, sslValidate: true,
                    sslCA: sslCertOptions.ca, sslKey: sslCertOptions.key, sslCert: sslCertOptions.cert
            };
        }
        else
        {
            return {useNewUrlParser: true, useUnifiedTopology: true, readPreference: 'primaryPreferred',
                    maxStalenessSeconds: 90, poolSize: 200, ssl: false, sslValidate: false
            };
        }
    }
    
    connectAndValidate(targetCredentials, sourceCredentials)
    {
        //declare and define common variables
        const mongodb         = require('mongodb');
        const util            = require('util');
        const uriTarget       = String('mongodb+srv://' + targetCredentials.dbUserName + ':' + targetCredentials.dbUserPassword
                                + '@' + targetCredentials.dbDomainURL + '/' + targetCredentials.dbName);
        const uriSource       = String('mongodb://' + sourceCredentials.dbUserName + ':' + sourceCredentials.dbUserPassword
                                + '@' + sourceCredentials.dbDomainURL + ':' + sourceCredentials.dbPort + '/' + sourceCredentials.dbName);
        //..connect to target
        mongodb.MongoClient.connect(uriTarget, targetCredentials.mongoDBConnectionOptions, function(targetConnectionError, targetClient)
        {
            if(targetConnectionError)
            {
                console.log(targetConnectionError);
                console.log("Target connection error: MongoDB-server is down or refusing connection.");
                return;
            }

            //get reference to target database and confirm connection
            console.log();
            const targetDb = targetClient.db(targetCredentials.dbName);
            console.log("............................................");
            console.log("Now connected to  target database on:", targetCredentials.dbDomainURL);
            console.log("............................................");
            
            //..connect to source
            mongodb.MongoClient.connect(uriSource, sourceCredentials.mongoDBConnectionOptions, function(sourceConnectionError, sourceClient)
            {
                if(sourceConnectionError)
                {
                    console.log(sourceConnectionError);
                    console.log("Source connection error: MongoDB-server is down or refusing connection.");
                    return;
                }
                
                //get reference to source database and confirm connection
                console.log();
                const sourceDb = sourceClient.db(sourceCredentials.dbName);
                console.log("............................................");
                console.log("Now connected to source database on:", sourceCredentials.dbDomainURL);
                console.log("............................................");
            
                
                //get all db statistics
                 targetDb.stats(function(targetStatsError, targetStats)
                 {
                      if(targetStatsError)
                      {
                          console.log(targetStatsError);
                          console.log("Error while getting Target Statistics.");
                          return;
                      }
                      
                      sourceDb.stats(function(sourceStatsError, sourceStats)
                      {
                        if(sourceStatsError)
                        {
                            console.log(sourceStatsError);
                            console.log("Error while getting Source Statistics.");
                            return;
                        }
                    
                        //finally validate
                        let mdbv = new MongoDBValidate();
                        let validate = mdbv.validate(targetStats, sourceStats);
                        
                        //then close connections to databases
                        targetClient.close();
                        sourceClient.close();
                        console.log("Connections to Target and Source Databases are now closed ........");
                        console.log();
                        
                      });
                  });
          });
        });
    }

    getRelevantStatistics(targetStat, sourceStat)
    {
        let targetValidationVariablesList = [];
        let sourceValidationVariablesList = [];
        
        //get db.stats' variables used (relevant) in validation
        //target
        targetValidationVariablesList.push(String(targetStat.db));
        targetValidationVariablesList.push(Number(targetStat.collections));
        targetValidationVariablesList.push(Number(targetStat.objects));
        targetValidationVariablesList.push(Number(targetStat.avgObjSize));
        targetValidationVariablesList.push(Number(targetStat.dataSize));
        targetValidationVariablesList.push(Number(targetStat.numExtents));
        targetValidationVariablesList.push(Number(targetStat.indexes));
        //source
        sourceValidationVariablesList.push(String(sourceStat.db));
        sourceValidationVariablesList.push(Number(sourceStat.collections));
        sourceValidationVariablesList.push(Number(sourceStat.objects));
        sourceValidationVariablesList.push(Number(sourceStat.avgObjSize));
        sourceValidationVariablesList.push(Number(sourceStat.dataSize));
        sourceValidationVariablesList.push(Number(sourceStat.numExtents));
        sourceValidationVariablesList.push(Number(sourceStat.indexes));
        return {"targetValidationVariablesList": targetValidationVariablesList, "sourceValidationVariablesList" : sourceValidationVariablesList};
    }
    
    removeUnusedStatistics(targetStat, sourceStat)
    {
      //remove db.stats' variables not used in validation
      //target
      delete targetStat.views;
      delete targetStat.indexSize;
      delete targetStat.storageSize;
      delete targetStat.scaleFactor;
      delete targetStat.fsUsedSize;
      delete targetStat.fsTotalSize;
      delete targetStat.ok;
      delete targetStat.nsSizeMB;
      delete targetStat.fileSize;
      //source
      delete sourceStat.views;
      delete sourceStat.indexSize;
      delete sourceStat.storageSize;
      delete sourceStat.scaleFactor;
      delete sourceStat.fsUsedSize;
      delete sourceStat.fsTotalSize;
      delete sourceStat.ok;
      delete sourceStat.nsSizeMB;
      delete sourceStat.fileSize;
      return {"targetStat" : targetStat,  "sourceStat": sourceStat};
    }
  
    testMongoDBValidate()
    {
        //instantiate the MongoDBValidate() class
        let mdbv = new MongoDBValidate();
        
        //target database credentials
        //note that: port is not required for Mongo Atlas target: it is already defined internally
        let targetDbName         = 'targetDbName'; 
        let targetUserName       = 'targetUserName';
        let targetUserPasd       = 'targetUserPasd ';
        let targetDbDomainURL    = 'targetDbDomainURL'; 
        let targetEnableSSL      = false;
        let targetSslCertOptions = undefined;
        if(targetEnableSSL === true)
        {
          let targetSslCertOptions = {"ca": "/pathToCAFile/caFile.pem" , "key": "/pathToKEYFile/keyFile.pem", "cert": "/pathToCERTFile/certFile.pem"};
        }
        let targetMongodbOptions = mdbv.mongoDBConnectionOptions(targetSslCertOptions, targetEnableSSL);
        
        //source database credentials
        let sourcePort           = '27017';
        let sourceDbName         = 'sourceDbName';
        let sourceUserName       = 'sourceUserName';
        let sourceUserPasd       = 'sourceUserPasd';
        let sourceDbDomainURL    = 'sourceDbDomainURL'; 
        let sourceEnableSSL      = false;
        let sourceSslCertOptions = undefined;
        if(sourceEnableSSL === true)
        {
          let sourceSslCertOptions = {"ca": "/pathToCAFile/caFile.pem" , "key": "/pathToKEYFile/keyFile.pem", "cert": "/pathToCERTFile/certFile.pem"};
        }
        let sourceMongodbOptions = mdbv.mongoDBConnectionOptions(sourceSslCertOptions, sourceEnableSSL);
        
        //define variables to hold credentials
        let targetCredentials    = {"dbPort": null, "dbName": targetDbName, "dbUserName": targetUserName, "dbUserPassword": targetUserPasd, "dbDomainURL": targetDbDomainURL};
        let sourceCredentials    = {"dbPort": sourcePort, "dbName": sourceDbName, "dbUserName": sourceUserName, "dbUserPassword": sourceUserPasd, "dbDomainURL": sourceDbDomainURL};
        
        //finally pass in credentials to connect and validate
        mdbv.connectAndValidate(targetCredentials, sourceCredentials);
    }
}

//test the class
//new MongoDBValidate().testMongoDBValidate();

// export module so that it can be used by other module or application
module.exports = {MongoDBValidate};




